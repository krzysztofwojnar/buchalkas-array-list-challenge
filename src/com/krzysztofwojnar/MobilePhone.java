package com.krzysztofwojnar;


import java.util.ArrayList;

public class MobilePhone {

    private ArrayList<Contact> contacts = new ArrayList<Contact>();
    public static final String INCORRECT_CHOICE = "Incorrect number. Try again. Type [5] to show help.";

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void mainMenu() {
        System.out.println("Welcome to Fuchsia OS - new OS for mobile phones. \nFuchsia is designed for modern smartphones with physical kayboard. This operating system has even phonebook. Have a nice day! :)");
        printHelpMessage();
        int choice;
        while (true) {
            if (Main.scanner.hasNextInt()) {
                choice = Main.scanner.nextInt();
            } else {
                choice = 7;
            }
            switch (choice) {
                case 0:
                    return;
                case 1:
                    if (isContactListEmpty()) {
                        break;
                    }
                    findContact();
                    break;
                case 2:
                    newContact();
                    break;
                case 3:
                    if (isContactListEmpty()) {
                        break;
                    }
                    modifyContact();
                    break;
                case 4:
                    if (isContactListEmpty()) {
                        break;
                    }
                    deleteContact();
                    break;
                case 5:
                    printHelpMessage();
                    break;
                case 6:
                    printWholeContactList ();
                    break;
                case 7:
                    System.out.println(INCORRECT_CHOICE);
                    break;
            }
            System.out.println("Type [5] to show help.");
            //Main.scanner.nextLine();
        }
    }

    private int searchContact() {
        int position = -1;
        System.out.println("Type contact name or phone number");
        String wantedString = Main.scanner.nextLine();
        if (Main.scanner.hasNextInt()) {
            int wantedInt = Main.scanner.nextInt();
            for (int i = 0; i < contacts.size(); i++) {
                if (contacts.get(i).getPhoneNumber() == wantedInt) {
                    position = i;
                    break;
                }
            }
            wantedString = Integer.toString(wantedInt);
        } else {
            wantedString = Main.scanner.nextLine();
        }
        for (int i = 0; i < contacts.size(); i++) {
            if (contacts.get(i).getName().equals(wantedString)) {
                position = i;
                break;
            }
        }

        if (position < 0) {
            System.out.println("Contact not found.");
            Main.scanner.nextLine();
            return -1;
        }
        return position;
    }

    public void findContact() {
        int position = searchContact();
        if (position == -1) return;
        System.out.println("Name: " + contacts.get(position).getName() + " \nPhone number: " + contacts.get(position).getPhoneNumber());
    }

    private Contact enterContact() {
        System.out.print("Enter contact name:");
        Main.scanner.nextLine();
        String name = Main.scanner.nextLine();
        int number;
        System.out.print("Enter contact number:");
        while (true) {
            if (Main.scanner.hasNextInt()) {
                number = Main.scanner.nextInt();
                break;
            } else {
                System.out.println("Type again");
                Main.scanner.nextLine();
            }
        }
        Main.scanner.nextLine();
        return new Contact(name, number);
    }

    private void newContact(String name, int number) {
        contacts.add(new Contact(name, number));
        System.out.println("Contact was added");
    }

    public void newContact() {
        Contact newContact = enterContact();
        boolean alreadyUsed = false;
        for (int i = 0; i < contacts.size(); i++) {
            if (contacts.get(i).getPhoneNumber() == newContact.getPhoneNumber()) {
                alreadyUsed = true;
                System.out.println("This phone number is used!");
                break;
            }
        }
        for (int i = 0; i < contacts.size(); i++) {
            if (contacts.get(i).getName().equals(newContact.getName())) {
                alreadyUsed = true;
                System.out.println("This contact name is used!");
                break;
            }
        }
        if (alreadyUsed) return;
        newContact(newContact.getName(), newContact.getPhoneNumber());
    }

    public void modifyContact() {
        System.out.println("Which contact do you want modify?");
        int position = searchContact();
        System.out.println("Enter new data");
        if (position < 0) return;
        Contact newContact = enterContact();
        //checking if data was used in another contact
        boolean wasUsedBefore;
        for (int i = 0; i < contacts.size(); i++) {
            if (i==position) continue;
            if (contacts.get(i).getPhoneNumber() == newContact.getPhoneNumber()) {
                wasUsedBefore = true;
                System.out.println("Number " + newContact.getPhoneNumber() + " was used before as a number of " + contacts.get(i).getName() + ".");
                //break;
                return;
            } else if (contacts.get(i).getName().equals(newContact.getName())) {
                wasUsedBefore = true;
                System.out.println("Contact name '" + newContact.getName() + "' was used before as a owner of phone number " + contacts.get(i).getPhoneNumber() + ".");
                //break;
                return;
            }

        }
        contacts.set(position, newContact);
    }

    public void deleteContact() {
        System.out.println("Which contact do you want delete?");
        int position = searchContact();
        System.out.println("Are you sure?(yes/no)");
        if (Main.scanner.nextLine().equals("y") || Main.scanner.nextLine().equals("yes")) {
            contacts.remove(position);
            System.out.println("Contact removed.");
        }
    }

    private void printHelpMessage() {
        System.out.println("Type number to perform action: " +
                "\n\r [1] to search contact \n\r [2] to add new contact " +
                "\n\r [3] to modify existing contact \n\r [4] to delete contact." +
                "\n\r [5] to show this help message \n\r [6] to print contact list \n\r [0] to quit");

    }

    private boolean isContactListEmpty() {
        if (contacts.size() <= 0) {
            System.out.println("Contact list is empty.");
            return true;
        } else return false;
    }
    public void printWholeContactList () {
        for (int i = 0; i < contacts.size(); i++) {
            System.out.println("Name: " + contacts.get(i).getName() + " Phone Number: " + contacts.get(i).getPhoneNumber());
        }
    }
}
