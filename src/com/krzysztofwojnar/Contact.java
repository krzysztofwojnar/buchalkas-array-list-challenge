package com.krzysztofwojnar;

public class Contact {
    public Contact(String name, int phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    private String name;
    private int phoneNumber;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
//  private ArrayList<Object> listsName = new ArrayList<Object>();
//  private ArrayList<Object> nextListsName = new ArrayList<Object>(listsName); //copying list by declaring new ArrayList
//  Object[] arraysName = new Object[listsName.size()];
//  arraysName = listsName.toArray(arraysName);
//
//  .add(Object);
//  .size();
//  .get(int position);
//  .set(int position, newObject);
//  .remove(int position);
//  .contains(searchItem); //boolean
//  .indexOf(searchItem); //-1 if there is none
//  .addAll(ArrayList); //adding ArrayList to ArrayList (copying)
//
//  So how do we go about doing this?
//  self-contained samodzielny
